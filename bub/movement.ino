
char cell(byte x, byte y) {
  if (x < 0 || x > 7 || y < 0 || y > 7) {
    return 0;
  }
  return level_data[(y * 8) + x];
}

byte isempty(char cell) {
  return (cell == ' ' || cell == '4') ? 1 : 0;
}

int moveto(int x, int y, int fromx, int fromy) {
  int stay = 0;

  char who = cell(fromx, fromy);

  if (fromx == ork_x && fromy == ork_y) {
    who = '@'; //special mode
  }

  char targ = cell(x, y);

  if (who != '@') {
    if (!isempty(targ)) {
      stay = 1;
    }
  } else {
    if (!targ || targ == '#' || targ == '=') {
      sfx(SOUND_NOPE, 0);
      return 0;
    } else if (targ == 'o') {
      if (bubs + keys < MAX_INVENTORY_ITEMS) {
        level_data[(y * 8) + x] = ' ';
        sfx(SOUND_EAT_BUB, 0);
        stay = 1;
        bubs++;
      } else {
        sfx(SOUND_NOPE, 0);
        return 0;
      }
    } else if (targ == '-') {
      if (bubs + keys < MAX_INVENTORY_ITEMS) {
        level_data[(y * 8) + x] = ' ';
        sfx(SOUND_EAT_KEY, 0);
        stay = 1;
        keys++;
      } else {
        sfx(SOUND_NOPE, 0);
        return 0;
      }
    } else if (targ == 'X') {
      if (!keys) {
        sfx(SOUND_PAIN, 0);
        return 0;
      }
      stay = 1;
      keys--;
      level_data[(y * 8) + x] = ' ';
      sfx(SOUND_UNLOCK, 0);
    }
  }

  if (stay) {
    return 0;
  }

  if (who != '@') {
    level_data[(y * 8) + x] = who;
    level_data[(fromy * 8) + fromx] = ' ';
    if (fromx == flag_x && fromy == flag_y) {
      level_data[(fromy * 8) + fromx] = '4';
    }
  } else {
    ork_x = x;
    ork_y = y;
    if (targ == '4') {
      flagbag = true;
      sfx(SOUND_FLAG, 0);
    }
  }

  return 1;
}

void fall(int fromx, int fromy) {
  char who = cell(fromx, fromy);

  if (fromx == ork_x && fromy == ork_y) {
    who = '@'; //special mode
  }

  int x = fromx;
  int y = fromy;
  int origy = y;
  char below = cell(x, y + 1);
  int fell = 0;

  if (cell(x, y) == 'H') {
    return;
  }

  while (below && isempty(below)) {
    if (moveto(x, y + 1, x, y)) {
      y++;
      fell++;
      below = cell(x, y + 1);
    } else {
      below = 0;
    }
  }

  if (fell) {
    sfx(SOUND_FALL, 0);
  }
}

void allfall() {
  int x, y;

  for (y = 7; y >= 0; y--) {
    for (x = 0; x < 8; x++) {
      if (cell(x, y) == '=' || (x == ork_x && y == ork_y)) {
        fall(x, y);
      }
    }
  }
}

int movedown() {
  int moved = 0;
  char targ = cell(ork_x, ork_y + 1);

  ork_sprite = SPRITE_ORK_DOWN;

  if (cell(ork_x, ork_y + 1) == 'H') {
    moveto(ork_x, ork_y + 1, ork_x, ork_y);
    sfx(SOUND_CLIMB, 0);
    return 0;
  } else if (cell(ork_x, ork_y) == '<' || cell(ork_x, ork_y) == '>') {
    sfx(SOUND_NOPE, 0);
    return 0;
  } else if (!cell(ork_x, ork_y + 1)) {
    if (cell(ork_x, ork_y) == 'H') {
      fall(ork_x, ork_y);
      return 0;
    }
  } else if (cell(ork_x, ork_y) == 'H') {
    if (isempty(targ)) {
      moved = moveto(ork_x, ork_y + 1, ork_x, ork_y);
      fall(ork_x, ork_y);
    }
    return moved;
  }

  if (!bubs && !keys) {
    fall(ork_x, ork_y);
    sfx(SOUND_NOPE, 0);
    return 0;
  }

  // plop and climb
  targ = cell(ork_x, ork_y - 1);
  if (!targ ||
      (!isempty(targ) && targ != 'H' && targ != '<' && targ != '>')) {
    return 0;
  }

  if (bubs) {
    level_data[(ork_y * 8) + ork_x] = 'o';
    bubs--;
    sfx(SOUND_PLOP, 0);
  } else if (keys) {
    level_data[(ork_y * 8) + ork_x] = '-';
    keys--;
    sfx(SOUND_PLOP, 0);
  }

  moveto(ork_x, ork_y - 1, ork_x, ork_y);
  fall(ork_x, ork_y);

  return 1;
}

void moveup() {
  int moved = 0;
  ork_sprite = SPRITE_ORK_UP;

  if (!cell(ork_x, ork_y - 1)) {
    sfx(SOUND_NOPE, 0);
    return;
  }

  if (cell(ork_x, ork_y) == 'H' &&
      (cell(ork_x, ork_y - 1) == 'H' || isempty(cell(ork_x, ork_y - 1)))) {
    moveto(ork_x, ork_y - 1, ork_x, ork_y);
    sfx(SOUND_CLIMB, 0);
    return;
  } else if (!cell(ork_x, ork_y + 1)) {
    moved = movedown();
  } else if (cell(ork_x, ork_y + 1) != 'H') {
    moved = movedown();
  }

  if (!moved) {
    ork_sprite = SPRITE_ORK_UP;
    sfx(SOUND_NOPE, 0);
  }
}

void moveleft() {
  char kick = cell(ork_x - 1, ork_y);

  ork_sprite = SPRITE_ORK_LEFT;

  if (!cell(ork_x - 1, ork_y) || cell(ork_x - 1, ork_y) == '>') {
    sfx(SOUND_NOPE, 0);
    return;
  }

  if (kick == '=') {
    ork_sprite = SPRITE_ORK_DOWN;
    sfx(SOUND_KICK, 0);
    moveto(ork_x - 2, ork_y, ork_x - 1, ork_y);
    allfall();
    return;
  }

  if (moveto(ork_x - 1, ork_y, ork_x, ork_y)) {
    sfx(SOUND_WALK, 0);
  }

  allfall();
}

void moveright() {
  char kick = cell(ork_x + 1, ork_y);

  ork_sprite = SPRITE_ORK_RIGHT;

  if (!cell(ork_x + 1, ork_y) || cell(ork_x + 1, ork_y) == '<') {
    sfx(SOUND_NOPE, 0);
    return;
  }

  if (kick == '=') {
    ork_sprite = SPRITE_ORK_DOWN;
    sfx(SOUND_KICK, 0);
    moveto(ork_x + 2, ork_y, ork_x + 1, ork_y);
    allfall();
    return;
  }

  if (moveto(ork_x + 1, ork_y, ork_x, ork_y)) {
    sfx(SOUND_WALK, 0);
  }

  allfall();
}
