  http://smogheap.github.io/bub/  

Directional buttons: play!
A: reset level
B: menu
Menu: title screen

When I saw the Gamebuino campaign on Indiegogo, I knew I had to have one.
This is the first game I've come up with specifically for the Gamebuino,
though my HTML5 prototype has sort of developed a life of its own too.

I hope you enjoy it!  It's GPL3, so please share and customize and send me
patches and do whatever you like with it.  :^)
