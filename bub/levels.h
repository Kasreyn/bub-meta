#pragma once

#define NB_LEVELS        13

const char PROGMEM levels[] = \
                              "        " \
                              " >      " \
                              "###     " \
                              "       4" \
                              "      ##" \
                              "oo @ ###" \
                              "########" \
                              "########" /* 0 */

                              "        " \
                              " >      " \
                              "###     " \
                              "       4" \
                              "      ##" \
                              "oo @    " \
                              "########" \
                              "########" /* 1 */

                              "        " \
                              "    4   " \
                              "  H###H " \
                              "  H   H " \
                              "  H     " \
                              "@ H o o " \
                              "########" \
                              "########" /* 2 */

                              "        " \
                              "    4   " \
                              "   ###H " \
                              "      H " \
                              "        " \
                              "@   o o " \
                              "########" \
                              "########" /* 3 */

                              "        " \
                              "        " \
                              "@       " \
                              "o       " \
                              "oo    4 " \
                              "ooo  ###" \
                              "oooo ###" \
                              "########" /* 4 */

                              "        " \
                              "        " \
                              "        " \
                              "        " \
                              "@     4 " \
                              "o    ###" \
                              "o    ###" \
                              "########" /* 5 */

                              "4       " \
                              "#H      " \
                              " H # o  " \
                              " H # o  " \
                              "######H#" \
                              "      H " \
                              "o @ # H " \
                              "########" /* 6 */

                              "@       " \
                              "#H      " \
                              "oH #    " \
                              "oH #    " \
                              "######H#" \
                              "      H " \
                              "  4 # H " \
                              "########" /* 7 */

                              "4       " \
                              "####### " \
                              " oo     " \
                              " #######" \
                              "     oo " \
                              "####### " \
                              "@oo     " \
                              "########" /* 8 */

                              "4       " \
                              "######o " \
                              "  o     " \
                              " o######" \
                              "     o  " \
                              "######o " \
                              "@ o     " \
                              "########" /* 9 */

                              "     4  " \
                              "    ###H" \
                              "o    ##H" \
                              "o @   XH" \
                              "#####H##" \
                              "#####H##" \
                              "##-  H##" \
                              "########" /* 10 */

                              "     4  " \
                              "    ###H" \
                              "o    ##H" \
                              "o @   XH" \
                              "#####H##" \
                              "#####H##" \
                              "##o  H##" \
                              "########" /* 11 */

                              "      4 " \
                              "    @ #H" \
                              "  # - #H" \
                              "  #####H" \
                              "    X  H" \
                              " o- #  H" \
                              "########" \
                              "########" /* 12 */

                              "     #4 " \
                              "  X @ #H" \
                              "  # oo#H" \
                              "  #####H" \
                              "    X  H" \
                              "-o- #   " \
                              "########" \
                              "########" /* 13 */

                              "        " \
                              "  =     " \
                              "H##     " \
                              "H       " \
                              "H@=   4 " \
                              "#### ###" \
                              "#### ###" \
                              "########" /* 14 */

                              "  =     " \
                              "  o     " \
                              "H##     " \
                              "H       " \
                              "H@    4 " \
                              "#### ###" \
                              "#### ###" \
                              "########" /* 15 */

                              "     4  " \
                              " H## ##H" \
                              " H #   H" \
                              " H #   H" \
                              "@H #  o " \
                              "#  #  o " \
                              "#  #   o" \
                              "########" /* 16 */

                              "   X 4X " \
                              " H## ##H" \
                              " H #  -H" \
                              " H #   H" \
                              "@H #  o " \
                              "o  #  o " \
                              "o -#   o" \
                              "########" /* 17 */

                              " -  4   " \
                              "H## # #H" \
                              "H   ###H" \
                              " -     H" \
                              " #H#   H" \
                              "  H @   " \
                              "#####H  " \
                              "oooXXH##" /* 18 */

                              " -  4 X " \
                              "H## # #H" \
                              "H   ###H" \
                              " -     H" \
                              " #H#   H" \
                              "  H @   " \
                              "#####H  " \
                              "oooXXH-#" /* 19 */

                              "    o   " \
                              "   o  # " \
                              "  o  ## " \
                              " o  ##  " \
                              "o  ##   " \
                              "o@##    " \
                              " ##     " \
                              "##4     " /* 20 */

                              "    o   " \
                              "   o  # " \
                              "  o  ## " \
                              " o  ##  " \
                              "-  ##   " \
                              "o@##    " \
                              " ##     " \
                              "##4X    " /* 21 */

                              "        " \
                              " o    # " \
                              "  o @ # " \
                              "####### " \
                              "        " \
                              " 4   =  " \
                              "### ##H#" \
                              "#   =oH#" /* 22 */

                              "-     X " \
                              " o    # " \
                              "  o @ # " \
                              "####### " \
                              "        " \
                              " 4   =  " \
                              "##  ##H#" \
                              "#   =oH#" /* 23 */

                              "########" \
                              "########" \
                              "########" \
                              "        " \
                              "o @ >  4" \
                              "###### #" \
                              "########" \
                              "########" /* 24 */

                              "########" \
                              "########" \
                              "########" \
                              "o   =   " \
                              "o @ >  4" \
                              "###### #" \
                              "###### #" \
                              "###### #" /* 25 */

                              "- <  >  " \
                              " ##H### " \
                              " @ H  o " \
                              "####  ##" \
                              "        " \
                              "  ######" \
                              "    X 4 " \
                              "### ####" /* 26 */

                              "- <  >  " \
                              " ##H### " \
                              " @ H  o " \
                              "####  ##" \
                              "        " \
                              "  ######" \
                              "    = 4 " \
                              "### X###" /* 27 */

                              "        " \
                              "     =  " \
                              "4 =  =  " \
                              "##=##=#H" \
                              "  =  = H" \
                              "  =  o H" \
                              "  o @  H" \
                              "########" /* 28 */

                              "        " \
                              "     =  " \
                              "4 =  =  " \
                              "##=# =#H" \
                              "  =  = H" \
                              "  =  o H" \
                              "  o @  H" \
                              "########" /* 29 */

                              " @=     " \
                              "H##     " \
                              "H       " \
                              "H##   4 " \
                              "H    ###" \
                              "H       " \
                              "H = o o " \
                              "HHHHH###" /* 30 */

                              "  =     " \
                              "H #     " \
                              "H@      " \
                              "H##   4 " \
                              "H    ###" \
                              "H       " \
                              "H = o o " \
                              "H#HHH###" /* 31 */

                              "        " \
                              " HHHHHH " \
                              " H    H " \
                              " H HH H " \
                              " H 4H H " \
                              " H  H H " \
                              " HHHH H " \
                              "      H@" /* 32 */

                              "        " \
                              " oooooo " \
                              " o    o " \
                              " o oo o " \
                              " o 4o o " \
                              " o  o o " \
                              " oooo o " \
                              "      o@" /* 33 */

                              " @=    -" \
                              "##HHH  o" \
                              "    ## o" \
                              "    #   " \
                              "    ####" \
                              " -   ###" \
                              "     XX4" \
                              "     ###" /* 34 */

                              " @=    o" \
                              " #HHH  o" \
                              "    ## o" \
                              "    #-  " \
                              " -  ####" \
                              "     ###" \
                              "     XX4" \
                              "     ###" /* 35 */

                              "########" \
                              "-   o   " \
                              " H ## H " \
                              "H # 4# H" \
                              " H#X #H " \
                              "H o#H  H" \
                              "H   @  H" \
                              "########" /* 36 */

                              "########" \
                              "o  #-   " \
                              " H ## H " \
                              "H # 4# H" \
                              " H#X #H " \
                              "H o#H  H" \
                              "H   @  H" \
                              "########" /* 37 */

                              "        " \
                              "        " \
                              "   @    " \
                              "  oooo  " \
                              " oo ooo " \
                              "oo ooooo" \
                              "ooooo4oo" \
                              "########" /* 38 */

                              "        " \
                              "        " \
                              "   @    " \
                              "  oooo  " \
                              "ooo<ooo " \
                              " o>ooooo" \
                              "ooooo4oo" \
                              "########" /* 39 */

                              " -  - o " \
                              " ----4o " \
                              " -  -oo " \
                              " ----@o " \
                              "  H  H  " \
                              "  H##H  " \
                              "  H  H  " \
                              "  HHHH  " /* 40 */

                              " o  o # " \
                              " oooo4# " \
                              " o  o## " \
                              " oooo@# " \
                              "  =  =  " \
                              "  =##=  " \
                              "  =  =  " \
                              "  ====  " /* 41 */

                              "########" \
                              "########" \
                              "#####   " \
                              "   ooooo" \
                              "@ ooooo4" \
                              "########" \
                              "########" \
                              "########" /* 42 */

                              "########" \
                              "########" \
                              "   #####" \
                              "  ooooo " \
                              "@oooooo4" \
                              "########" \
                              "########" \
                              "########" /* 43 */

                              "####    " \
                              "- @    o" \
                              "####    " \
                              "   # H##" \
                              "   # H o" \
                              "  =X  H " \
                              " ###    " \
                              "  4#####" /* 44 */

                              "####    " \
                              "- @    o" \
                              "####    " \
                              " o # H##" \
                              "oo # H o" \
                              "  =X  H " \
                              " ###    " \
                              " o4#####" /* 45 */

                              "###### @" \
                              "   Xoo #" \
                              " o#### 4" \
                              "oo   # H" \
                              "###H # H" \
                              "    -# H" \
                              "H#####  " \
                              "H    <  " /* 46 */

                              "###### @" \
                              "    o# #" \
                              "  #### 4" \
                              "oo   o #" \
                              "###H # H" \
                              "    o# H" \
                              "H#####  " \
                              "H       " /* 47 */

                              "@o      " \
                              "Ho= = =4" \
                              "Ho= = = " \
                              "Ho= = = " \
                              "Ho= = = " \
                              "Ho= = = " \
                              "Ho= = = " \
                              "Ho= = = " /* 48 */

                              "@o      " \
                              "Ho= = = " \
                              "Ho= = =4" \
                              "H = = = " \
                              "H== = = " \
                              "H== = = " \
                              "H==== = " \
                              "H==== = " /* 49 */

                              "   -    " \
                              "  ooo   " \
                              " ooooo  " \
                              "#     #H" \
                              " # X # H" \
                              "  #4#  H" \
                              "   #   H" \
                              "@      H" /* 50 */

                              "   -    " \
                              "  ooo   " \
                              " o   o  " \
                              "#     #H" \
                              " # X # H" \
                              "  #4#  H" \
                              "   #   H" \
                              "@  o   H" /* 51 */

                              "   H 4 #" \
                              "   H ###" \
                              "   H    " \
                              "   H#   " \
                              "   H    " \
                              "   H    " \
                              "  oH    " \
                              "  oH@   " /* 52 */

                              "   H 4 #" \
                              "   H ###" \
                              "   H    " \
                              "   H    " \
                              "   H    " \
                              "   H    " \
                              "  oH    " \
                              "  oH@   " /* 53 */

                              "#       " \
                              "##     #" \
                              "###   ##" \
                              "4X   ###" \
                              "###H = @" \
                              "## H ###" \
                              "#     ##" \
                              "  ooo -#" /* 54 */

                              "#       " \
                              "##     #" \
                              "###   ##" \
                              "4X   ###" \
                              "###H = @" \
                              "## H ###" \
                              "#     ##" \
                              "  oo-  #" /* 55 */

                              "  X4    " \
                              "#H###   " \
                              " H      " \
                              " H      " \
                              " H     -" \
                              "      o " \
                              " @  oooo" \
                              "########" /* 56 */

                              "  X4    " \
                              "#####   " \
                              " H      " \
                              " H      " \
                              " H     -" \
                              "      o " \
                              " @  oooo" \
                              "########" /* 57 */

                              "      = " \
                              " =   ##H" \
                              "H##    H" \
                              "H  = @ H" \
                              "H# # # H" \
                              "H##### H" \
                              "H ###- H" \
                              "H X4#  H" /* 58 */

                              "   -  = " \
                              " -   ##H" \
                              "H##o  oH" \
                              "H    @ H" \
                              "H#   # H" \
                              "H## ## H" \
                              "H ###- H" \
                              "HXXX4# H" /* 59 */

                              " HHHHH  " \
                              " H  = = " \
                              " H  ####" \
                              " H=     " \
                              " H#     " \
                              "@H     4" \
                              "###   ##" \
                              "########" /* 60 */

                              " HHHHH  " \
                              " H  = o " \
                              " H  ####" \
                              " H=     " \
                              " H#     " \
                              "@H     4" \
                              "###   ##" \
                              "###  ###" /* 61 */

                              " #  -   " \
                              " #      " \
                              " X   #H#" \
                              " ###  H " \
                              " #  @ H " \
                              " # o# H " \
                              "4# o  H " \
                              "########" /* 62 */

                              " ## -   " \
                              " ##    o" \
                              " XX  #H#" \
                              " ###  H " \
                              " #- @ H " \
                              " # o# H " \
                              "4#    H " \
                              "########" /* 63 */

                              " o      " \
                              " o=   = " \
                              "H##  ##H" \
                              "Ho     H" \
                              "H   ####" \
                              "H@     4" \
                              "## # ###" \
                              "## # ###" /* 64 */

                              " o    - " \
                              " o=   = " \
                              "H##  ##H" \
                              "H-     H" \
                              "H   ####" \
                              "H@   XX4" \
                              "## # ###" \
                              "## # ###" /* 65 */

                              "4ooooooo" \
                              "oo  oooo" \
                              " oooo o " \
                              "o oooo o" \
                              "ooo oooo" \
                              "ooo o oo" \
                              " ooooooo" \
                              " oo@oo o" /* 66 */

                              "4ooooooo" \
                              "oo  oooo" \
                              " #ooo o " \
                              "o oooo o" \
                              "ooo oooo" \
                              "ooo o oo" \
                              " ooooooo" \
                              " oo@oo o" /* 67 */

                              "    4   " \
                              "    H   " \
                              " o o o  " \
                              " # # # #" \
                              "o o o   " \
                              "# # # # " \
                              " @   oo " \
                              "########" /* 68 */

                              "    4   " \
                              "    H   " \
                              "o       " \
                              " # # # #" \
                              "o o     " \
                              "# # # # " \
                              " @   oo " \
                              "########" /* 69 */

                              "    =   " \
                              "    =   " \
                              " =  =   " \
                              "H# H# ##" \
                              "H  H  X4" \
                              "H# H# ##" \
                              "H@ H   -" \
                              "## ## ##" /* 70 */

                              " =  =   " \
                              " =  =   " \
                              " =  =   " \
                              "H# H# ##" \
                              "H  H  X4" \
                              "H# ##  #" \
                              "H@ o   -" \
                              "## ## ##" /* 71 */

                              "       4" \
                              "     ###" \
                              "   o o  " \
                              "   o    " \
                              "     o  " \
                              " o      " \
                              "@   o   " \
                              "########" /* 72 */

                              "       4" \
                              "     ###" \
                              "      o " \
                              "    ooo " \
                              "  o oo  " \
                              " oo     " \
                              "@   o   " \
                              "########" /* 73 */

                              "o   # 4 " \
                              " H# # # " \
                              "H - # X " \
                              "H   o##H" \
                              " H o   H" \
                              "H o    H" \
                              "H      @" \
                              "########" /* 74 */

                              "    # 4 " \
                              " H# # # " \
                              "H - # X " \
                              "H   o##H" \
                              " H o   H" \
                              "H o    H" \
                              "H      @" \
                              "########" /* 75 */

                              "@      4" \
                              "##o   ##" \
                              "  o     " \
                              "  o     " \
                              "  o     " \
                              "  o     " \
                              "  o     " \
                              "  o     " /* 76 */

                              "@      4" \
                              "##    ##" \
                              "        " \
                              "        " \
                              "        " \
                              "        " \
                              "        " \
                              "ooooooo " /* 77 */

                              "     =  " \
                              "   ##=# " \
                              "  ###=##" \
                              " o###=##" \
                              " =@  o 4" \
                              " o### ##" \
                              "   ## # " \
                              "o       " /* 78 */

                              "     =  " \
                              "   ##=# " \
                              "  ###=##" \
                              " o###=##" \
                              " =@  oX4" \
                              " o### ##" \
                              "   ## # " \
                              "o    -  " /* 79 */

                              "@ -X-X-X" \
                              "X--XX-- " \
                              " X---XXX" \
                              "####### " \
                              "----    " \
                              "  -     " \
                              "  ######" \
                              "  XXXXX4" /* 80 */

                              "@o-X-X-X" \
                              "X--XX-- " \
                              " X---XXX" \
                              "####### " \
                              "----    " \
                              "  -     " \
                              "       #" \
                              "  XXXXX4" /* 81 */

                              " XXXXXX4" \
                              "H#######" \
                              "H  --   " \
                              "H    -  " \
                              "H   --  " \
                              "H   -   " \
                              "o       " \
                              "@   o   " /* 82 */

                              " XX>XXX4" \
                              "H#######" \
                              "H  --   " \
                              "H    -  " \
                              "H   <-  " \
                              "H   -   " \
                              "o       " \
                              "@   o   " /* 83 */

                              "@  > o  " \
                              "#oH#  o " \
                              "##### # " \
                              " 4#=### " \
                              "H##=  < " \
                              "H#=X ## " \
                              "  o H-# " \
                              "  # H###" /* 84 */

                              "@  > o  " \
                              "#-H#  - " \
                              "##### # " \
                              " 4#=### " \
                              "H##=  < " \
                              " #oX ## " \
                              "  o H-X " \
                              "  # Ho##" /* 85 */

                              "@      4" \
                              "oooooo  " \
                              "        " \
                              " ooooooo" \
                              "        " \
                              "ooooooo " \
                              "        " \
                              " ooooooo" /* 86 */

                              "@      4" \
                              "o#o#o#  " \
                              "        " \
                              " ooo#o#o" \
                              "        " \
                              "o#o#ooo " \
                              "        " \
                              " ooo#o#o" /* 87 */

                              "    4   " \
                              "    H   " \
                              "        " \
                              " o     o" \
                              " o # # o" \
                              "o  # #o " \
                              " H## ##H" \
                              " H <@> H" /* 88 */

                              "    4   " \
                              "        " \
                              "        " \
                              " o     o" \
                              " o = = o" \
                              "o  # #o " \
                              " Hoo ooH" \
                              " H <@> H" /* 89 */

                              " 4 ##   " \
                              " -###   " \
                              "   ##@  " \
                              "  # ##  " \
                              "  # ##  " \
                              "  # ### " \
                              "   oo   " \
                              "  oooo  " /* 90 */

                              " @ o#   " \
                              " ####   " \
                              "  -##   " \
                              "  # ##  " \
                              "  # ##4 " \
                              "  # ### " \
                              "   oo   " \
                              "  oooo  " /* 91 */

                              "####@###" \
                              "### H ##" \
                              "4X  H  X" \
                              "### H H#" \
                              "- # H Ho" \
                              "#H oo H#" \
                              "#H    H#" \
                              "#H######" /* 92 */

                              "####@###" \
                              "### H ##" \
                              "4X  H  X" \
                              "### H H#" \
                              "- # H Ho" \
                              "#H  o H#" \
                              "#H    H#" \
                              "#H######" /* 93 */

                              "    = 4 " \
                              "  H## #H" \
                              " oHo# #H" \
                              " o o< #H" \
                              "H#### #H" \
                              "H  >=  H" \
                              "H####  #" \
                              "H @##  #" /* 94 */

                              "-   = 4 " \
                              "  H## #H" \
                              " -H-# #H" \
                              " o o< #H" \
                              "H###  #H" \
                              "H  >=  X" \
                              "H####  #" \
                              "H @##  #" /* 95 */

                              "-   # - " \
                              "  o X   " \
                              "  # ###H" \
                              "H###oHHH" \
                              "H   # # " \
                              "H  o# ##" \
                              " @ o# X4" \
                              "########" /* 96 */

                              "-   # - " \
                              "  o X   " \
                              "  # ### " \
                              "H###oHH " \
                              "H     H " \
                              "H  o  ##" \
                              " @ o  X4" \
                              "##### ##" /* 97 */

                              "       4" \
                              "      ##" \
                              "        " \
                              "    ##  " \
                              "        " \
                              "  ## ooo" \
                              "@    ooo" \
                              "########" /* 98 */

                              "-     X4" \
                              "      ##" \
                              "        " \
                              "    ##  " \
                              "        " \
                              "  ## ooo" \
                              "@    ooo" \
                              "########"; /* 99 */
