#pragma once

extern const byte font5x7[];

void vfx_flag() {
  byte r, g, b;
  char str[] = "Wahooooooooo!";

  for (byte i = 1; i < 5; i++) {
    r = random (128, 255);
    g = random (1, 255);
    b = r / 2;
    gb.lights.fill(gb.createColor(r, g, b));
    delay(50);
    gb.lights.clear();
    delay(50);
  }

  gb.display.setColor(BLACK);
  gb.display.fillRect(0, 55, gb.display.width(), 10);
  gb.display.cursorX = 0;
  gb.display.cursorY = 55;
  gb.display.setFont(font5x7);

  for (byte i = 0; i < strlen(str); i++) {
    gb.display.setColor(Gamebuino_Meta::defaultColorPalette[random(1, 15)]);
    gb.display.print(str[i]);
    delay(5 * i);
  }
}

void vfx_eat_a_bub() {
  //gb.lights.fill(gb.createColor(68, 133, 207));
  gb.lights.fill(gb.createColor(20, 20, 255));
  delay(50);
  gb.lights.clear();
}

void vfx_key_get() {
  gb.lights.fill(gb.createColor(245, 231, 50));
  delay(50);
  gb.lights.clear();
}

void vfx_unlock() {
  //gb.lights.fill(gb.createColor(207, 68, 133));
  gb.lights.fill(gb.createColor(145, 131, 0));
  delay(50);
  gb.lights.clear();
}

void vfx_nope() {
  gb.lights.fill(gb.createColor(219, 29, 35));
  delay(50);
  gb.lights.clear();
}
