#include <Gamebuino-Meta.h>
#include <Gamebuino-EEPROM.h>
#include <utility/Graphics.h>

#include "sprites.h"
#include "levels.h"
#include "sounds.h"

#define MAX_INVENTORY_ITEMS   2

extern const byte font3x5[];
extern const byte font5x7[];

/* game variables */
byte level = 0;
byte ork_sprite = SPRITE_ORK_LEFT;
byte ork_x = 0;
byte ork_y = 0;
byte flag_x = 0;
byte flag_y = 0;
byte bubs = 0;
byte keys = 0;
byte eviltwin = 0;
byte title_screen_music = 2;
byte levels_done[NB_LEVELS];
byte level_data[64];

boolean flagbag = false;
boolean title_screen_active = true;
boolean title_screen_color = true;

void showtitle() {
  if (gb.update()) {

    if (gb.buttons.pressed(BUTTON_A)) {
      title_screen_active = false;
    }

    if (gb.buttons.pressed(BUTTON_B)) {
      title_screen_color = !title_screen_color;
      if (++title_screen_music > 2) title_screen_music = 0 ;

      gb.sound.stop(sound_id_music);
      sound_id_music = gb.sound.play(music[title_screen_music], true);
    }

    if (title_screen_color) {
      gb.display.clear(BLACK);
      title_screen_color_sheet.setFrame(0);
      gb.display.drawImage(8, 10, title_screen_color_sheet);
    }
    else {
      gb.display.clear(BLACK);
      gb.display.setColor(WHITE);
      gb.display.drawBitmap(8, 10, title_screen_sprite_bw, NOROT, NOFLIP);
    }
  }
}

void readEEPROM()
{
  for (byte i = 0; i < NB_LEVELS; i++) {
    levels_done[i] = EEPROM.read(i);
  }
  eviltwin = EEPROM.read(NB_LEVELS);
}

void writeEEPROM() {
  for (byte i = 0; i < NB_LEVELS; i++) {
    EEPROM.write(i, levels_done[i]);
  }
  EEPROM.write(NB_LEVELS, eviltwin);
}

void loadlevel(byte level) {
  for (byte i = 0; i < 64; i++) {
    level_data[i] = pgm_read_byte(levels + (level * 64) + i);
  }
  bubs = 0;
  keys = 0;
  flagbag = false;
}

void next_level() {
  setLevelDone(level++);
  if (!eviltwin && level % 2) {
    level++;
  }
  if (level >= 99) {
    level = 0;
  }
  writeEEPROM();
  loadlevel(level);
}

void setLevelDone(byte level)
{
  byte i = level / 8;
  byte bitShift = level % 8;
  levels_done[i] |= 1 << bitShift;
}

boolean isLevelDone(byte level)
{
  byte i = level / 8;
  byte bitShift = level % 8;
  return (((levels_done[i] >> bitShift) & 0x01) == 0x01);
}

void refreshMenu(boolean evilSelected, byte selectedLevel) {
  gb.display.clear(BLACK);
  gb.display.setColor(WHITE);
  gb.display.setFont(font3x5);
  gb.display.cursorX = 0;
  gb.display.cursorY = 5;
  gb.display.print("Level Menu\n\n");
  gb.display.print("Level:\n");
  gb.display.print("Evil Twin:");

  /* Print level selection */
  gb.display.cursorX = 46;
  gb.display.cursorY = 17;
  gb.display.print(!evilSelected ? "< " : "  ");
  if (selectedLevel < 10)
    gb.display.print(" ");
  gb.display.print(selectedLevel);
  gb.display.print(!evilSelected ? "> " : "  ");
  gb.display.drawBitmap(70, 16, isLevelDone(selectedLevel) ? ok : ko);

  /* Print evil selection */
  gb.display.cursorX = 46;
  gb.display.cursorY = 23;
  gb.display.print(evilSelected ? "<" : " ");
  gb.display.print(eviltwin ? " ON" : "OFF");
  gb.display.print(evilSelected ? ">" : " ");
}

void showmenu() {
  boolean evilSelected = false;
  byte selectedLevel = level;

  while (1) {
    if (gb.update()) {
      if (gb.buttons.pressed(BUTTON_A)) {
        level = selectedLevel;
        loadlevel(selectedLevel);
        break;
      }

      if (gb.buttons.pressed(BUTTON_B)) {
        writeEEPROM();
        break;
      }

      if (gb.buttons.pressed(BUTTON_RIGHT)) {
        if (evilSelected) {
          eviltwin = !eviltwin;
        } else {
          selectedLevel++;
          if (!eviltwin && selectedLevel % 2)
            selectedLevel++;
          if (selectedLevel >= 99)
            selectedLevel = 0;
        }
      }

      if (gb.buttons.pressed(BUTTON_LEFT)) {
        if (evilSelected) {
          eviltwin = !eviltwin;
        } else {
          selectedLevel--;
          if (!eviltwin && selectedLevel % 2)
            selectedLevel--;
          if (selectedLevel < 0)
            selectedLevel = 99;
        }
      }

      if (gb.buttons.pressed(BUTTON_UP) || gb.buttons.pressed(BUTTON_DOWN)) {
        evilSelected = !evilSelected;
      }

      refreshMenu(evilSelected, selectedLevel);
    }
  }
}

void setup() {
  gb.begin();

  EEPROM.begin(256);
  readEEPROM();

  byte i = 0;
  while (i < 100 && isLevelDone(i))
    i += eviltwin ? 1 : 2;
  level = (100 == i) ? 0 : i;
  loadlevel(level);

  gb.sound.stop(sound_id_music);
  sound_id_music = gb.sound.play(music[title_screen_music], true);
}

void loop() {
  byte i, j, block_sprite;

  if (title_screen_active) {
    showtitle();
    return;
  }

  if (gb.update()) {
    if (flagbag) {
      vfx_flag();
      next_level();
    }

    if (gb.buttons.pressed(BUTTON_MENU)) {
      title_screen_active = true;
    }

    if (gb.buttons.pressed(BUTTON_B)) {
      gb.display.setFont(font3x5);
      showmenu();
    }

    if (gb.buttons.pressed(BUTTON_A)) {
      loadlevel(level);
      sfx(SOUND_RESTART, 0);
    }

    if (gb.buttons.pressed(BUTTON_LEFT)) {
      moveleft();
    }

    if (gb.buttons.pressed(BUTTON_RIGHT)) {
      moveright();
    }

    if (gb.buttons.pressed(BUTTON_UP)) {
      moveup();
    }

    if (gb.buttons.pressed(BUTTON_DOWN)) {
      movedown();
    }
  }

  /* set screen */
  gb.display.clear(BLACK);
  gb.display.setColor(WHITE, BLACK);

  /* draw level */
  for (i = 0; i < 8; i++) {
    for (j = 0; j < 8; j++) {
      block_sprite = SPRITE_EMPTY;
      switch (level_data[(i * 8) + j]) {
        case 'o':
          block_sprite = SPRITE_BUBBLE;
          break;
        case '#':
          block_sprite = SPRITE_WALL;
          break;
        case 'H':
          block_sprite = SPRITE_LADDER;
          break;
        case '-':
          block_sprite = SPRITE_KEY;
          break;
        case 'X':
          block_sprite = SPRITE_DOOR;
          break;
        case '>':
          block_sprite = SPRITE_ARROW_RIGHT;
          break;
        case '<':
          block_sprite = SPRITE_ARROW_LEFT;
          break;
        case '=':
          block_sprite = SPRITE_CRATE;
          break;
        case '4':
          block_sprite = SPRITE_FLAG;
          flag_x = j;
          flag_y = i;
          break;
        case '@':
          ork_x = j;
          ork_y = i;
          ork_sprite = SPRITE_ORK_LEFT;
          level_data[(i * 8) + j] = ' ';
          break;
        default:
          break;
      }

      sprites_sheet.setFrame(block_sprite);
      gb.display.drawImage(j * TILE_W, i * TILE_H + 5, sprites_sheet);
    }
  }

  sprites_sheet.setFrame(ork_sprite);
  gb.display.drawImage(ork_x * TILE_W, ork_y * TILE_H + 5, sprites_sheet);

  // draw UI
  gb.display.setFont(font3x5);
  gb.display.cursorX = 0;
  gb.display.cursorY = 55;
  gb.display.print("Level ");
  gb.display.print(level);

  // draw inventory box followed by bubbles and keys
  gb.display.drawRect(68, 16, TILE_W + 4, (TILE_H * MAX_INVENTORY_ITEMS) + 4);
  i = 18;
  for (j = 0; j < bubs; j++) {
    sprites_sheet.setFrame(SPRITE_BUBBLE);
    gb.display.drawImage(70, i, sprites_sheet);
    i += TILE_H;
  }
  for (j = 0; j < keys; j++) {
    sprites_sheet.setFrame(SPRITE_KEY);
    gb.display.drawImage(70, i, sprites_sheet);
    i += TILE_H;
  }

  /*
    for (i = j = 0; i < 16; i++) {
      indexed_colors_sprites_sheet.setFrame(i);
      gb.display.drawImage(i * 4, 60, indexed_colors_sprites_sheet);
    }
  */
}
