#pragma once

#include "visual_effects.h"

#define SOUND_CLIMB     0
#define SOUND_EAT_BUB   1
#define SOUND_EAT_KEY   2
#define SOUND_KICK      3
#define SOUND_WALK      4
#define SOUND_FALL      5
#define SOUND_NOPE      6
#define SOUND_PAIN      7
#define SOUND_PLOP      8
#define SOUND_FLAG      9
#define SOUND_UNLOCK    10
#define SOUND_RESTART   11

int8_t sound_id_music = -1;
int8_t sound_id_fx = -1;

const char* music[] = {
  "audio/8b-music01.wav", // 0
  "audio/8b-music02.wav", // 1
  "audio/8b-music03.wav", // 2
};

void sfx(byte fxno, byte channel) {
  switch (fxno) {
    case SOUND_CLIMB:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/climb01.wav", false);
      break;
    case SOUND_EAT_BUB:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/slurp01.wav", false);
      vfx_eat_a_bub();
      break;
    case SOUND_EAT_KEY:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/slurp01.wav", false);
      vfx_key_get();
      break;
    case SOUND_KICK:
      break;
    case SOUND_WALK:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/step01.wav", false);
      break;
    case SOUND_FALL:
      /*gb.sound.play(sound_fall);*/
      break;
    case SOUND_NOPE:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/cough01.wav", false);
      vfx_nope();
      break;
    case SOUND_PAIN:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/oof01.wav", false);
      break;
    case SOUND_PLOP:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/plop01.wav", false);
      break;
    case SOUND_FLAG:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/flag01.wav", false);
      break;
    case SOUND_UNLOCK:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/unlock01.wav", false);
      vfx_unlock();
      break;
    case SOUND_RESTART:
      gb.sound.stop(sound_id_fx);
      sound_id_fx = gb.sound.play("audio/reset.wav", false);
      break;
    default:
      break;
  }
}
