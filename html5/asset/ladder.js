LOAD({
	"name": "ladder",
	"scale": 0.27,
	"above": [
		{
			"name": "ladder",
			"image": "image/scribble/ladder.png",
			"pivot": {
				"x": 140,
				"y": 300
			},
			"rotate": -1,
			"scale": 1,
			"alpha": 1,
			"offset": {
				"x": 0,
				"y": -30
			},
			"above": [],
			"below": [
				{
					"name": "top",
					"image": "image/scribble/ladder.png",
					"pivot": {
						"x": 160,
						"y": 298
					},
					"rotate": 0,
					"scale": 1,
					"alpha": 1,
					"offset": {
						"x": 165,
						"y": 118
					},
					"above": [],
					"below": [
					]
				}
			]
		}
	],
	"below": [],
	"pose": {}
});
