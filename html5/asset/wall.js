LOAD({
	"name": "wall",
	"scale": 0.3,
	"above": [
		{
			"name": "brick",
			"image": "image/scribble/wall.png",
			"pivot": {
				"x": 200,
				"y": 440
			},
			"rotate": 0.5,
			"scale": 1,
			"alpha": 1,
			"offset": {
				"x": 0,
				"y": 0
			}
		}
	],
	"below": [
	],
	"pose": {}
});
