LOAD({
	"name": "edge",
	"scale": 0.34,
	"above": [
		{
			"name": "line",
			"image": "image/scribble/edge.png",
			"pivot": {
				"x": 215,
				"y": 385
			},
			"rotate": 4.5,
			"scale": 1,
			"alpha": 1,
			"offset": {
				"x": 0,
				"y": 0
			}
		}
	],
	"below": [
	],
	"pose": {}
});
